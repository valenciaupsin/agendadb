package database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public class AgendaDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = "INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_CONTACTO = "CREATE TABLE"+DefinirTabla.Contacto.TABLE_NAME +
            " ("+ DefinirTabla.Contacto._ID + "INTEGER PRIMARY KEY, "+
            DefinirTabla.Contacto.COLUMN_NAME_NOMBRE+TEXT_TYPE+COMMA_SEP+
            DefinirTabla.Contacto.COLUMN_NAME_DIRECCION+TEXT_TYPE+COMMA_SEP+
            DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1+TEXT_TYPE+COMMA_SEP+
            DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2+TEXT_TYPE+COMMA_SEP+
            DefinirTabla.Contacto.COLUMN_NAME_NOTAS+TEXT_TYPE+COMMA_SEP+
            DefinirTabla.Contacto.COLUMN_NAME_FAVORITE+INTEGER_TYPE+")";
    private static final String SQL_DELETE_CONTACTO = "DROPTABLE IF EXISTS"+DefinirTabla.Contacto.TABLE_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "agenda.db";


    public AgendaDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db, oldVersion,newVersion);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);
    }

}
