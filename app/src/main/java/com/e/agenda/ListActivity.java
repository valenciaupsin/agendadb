package com.e.agenda;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.AgendaContacto;
import database.Contacto;


public class ListActivity extends android.app.ListActivity {

    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;
    private Button btnNuevo;
    private AgendaContacto agendaContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        btnNuevo = findViewById(R.id.btnNuevo);
        agendaContacto = new AgendaContacto(this);

        llenarlista();
        adapter = new MyArrayAdapter(this, R.layout.layout_contacto,listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    private void llenarlista() {
        agendaContacto.openDatabase();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrar();
    }

    class MyArrayAdapter extends ArrayAdapter<Contacto> {
        Context context;
        int textViewResourceId;
        ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int resource, ArrayList<Contacto> contactos) {
            super(context,resource,contactos);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos= contactos;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView) view. findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView)view.findViewById(R.id.lblTelefonoContacto);
            Button btnModificar = (Button) view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button) view.findViewById(R.id.btnBorrar);
            if (contactos.get(position).getFavorite() > 0) {
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }
            else {
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnModificar.setOnClickListener((v) -> {
                Bundle bundle = new Bundle();
                bundle.putSerializable("contacto", contactos.get(position));
                Intent i = new Intent();
                i.putExtras(bundle);
                setResult(Activity.RESULT_OK,i);
                finish();
            });

            btnBorrar.setOnClickListener((v) -> {
                AgendaContacto agendaContacto = null;
                agendaContacto.openDatabase();
                agendaContacto.eliminarContacto(contactos.get(position).get_ID());
                agendaContacto.cerrar();
                contactos.remove(position);
                notifyDataSetChanged();
            });
            return view;
        }
    }
}
